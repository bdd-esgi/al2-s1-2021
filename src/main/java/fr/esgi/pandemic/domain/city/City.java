package fr.esgi.pandemic.domain.city;

public class City {
    private final CityName name;
    private int infectionLevel;

    public City(CityName name) {
        this.name = name;
        this.infectionLevel = 0;
    }

    public void setInfectionLevel(int infectionLevel) {
        this.infectionLevel = infectionLevel;
    }

    public int infectionLevel() {
        return infectionLevel;
    }

    public CityName name() {
        return name;
    }

    @Override
    public String toString() {
        return "City{" +
                "name=" + name +
                ", infectionLevel=" + infectionLevel +
                '}';
    }

    public void infect() {
        if (infectionLevel < 3) {
            infectionLevel++;
        }
    }
}
