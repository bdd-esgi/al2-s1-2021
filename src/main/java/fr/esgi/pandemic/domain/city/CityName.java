package fr.esgi.pandemic.domain.city;

public enum CityName {
    PARIS,
    LONDON,
    ESSEN,
    ALGIERS,
    MADRID,
    MILAN,
    NEW_YORK,
}
