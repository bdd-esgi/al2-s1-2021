package fr.esgi.pandemic.features;

import fr.esgi.pandemic.domain.city.CityName;

import java.util.Objects;

public record CityLink(CityName cityName1, CityName cityName2) {

    public boolean contains(CityName name) {
        return cityName1 == name || cityName2 == name;
    }

    public CityName other(CityName name) {
        if (name == cityName1){
            return cityName2;
        }
        else if (name == cityName2){
            return cityName1;
        }
        throw new IllegalArgumentException(name + " not existing in " + this);
    }

    @Override
    public String toString() {
        return cityName1 + " <---->" + cityName2;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        CityLink cityLink = (CityLink) o;
        return cityName1 == cityLink.cityName1 && cityName2 == cityLink.cityName2
                || cityName1 == cityLink.cityName2 && cityName2 == cityLink.cityName1;
    }

    @Override
    public int hashCode() {
        return Objects.hash(cityName1, cityName2);
    }
}
