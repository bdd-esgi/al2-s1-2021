Feature: Standard definitions

#             (LONDON)------------------(ESSEN)
#               | \                     /  |
#               |    \                /    |
#               |       \           /      |
#  (NEW-YORK)   |          (PARIS)         |
#               |       /    |     \       |
#               |    /       |        \    |
#               | /          |           \ |
#            (MADRID)        |          (MILAN)
#                \           |
#                   \        |
#                      \     |
#                         \  |
#                         (ALGIERS)
  @Occident
  Scenario: Occident network definition
    * the cities should have the following infection levels:
      | cityName | infection level |
      | Paris    | 0               |
      | London   | 0               |
      | Essen    | 0               |
      | Algiers  | 0               |
      | Madrid   | 0               |
      | Milan    | 0               |
      | New_york | 0               |
    * Paris should be linked to London, Essen, Milan, Algiers, Madrid
    * Essen should be linked to London, Paris, Milan
    * Madrid should be linked to London, Paris, Algiers