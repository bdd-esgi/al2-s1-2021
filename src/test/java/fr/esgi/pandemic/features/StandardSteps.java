package fr.esgi.pandemic.features;

import fr.esgi.pandemic.domain.city.City;
import fr.esgi.pandemic.domain.city.CityName;
import io.cucumber.datatable.DataTable;
import io.cucumber.java.ParameterType;
import io.cucumber.java.en.And;
import io.cucumber.java.en.Given;
import io.cucumber.java.en.Then;
import org.assertj.core.api.Assertions;

import java.util.*;

public class StandardSteps {

    private final ScenarioContext context;
    private StandardDefinitionHook standardDefinitionHook;

    public StandardSteps(ScenarioContext context, StandardDefinitionHook standardDefinitionHook) {
        this.context = context;
        this.standardDefinitionHook = standardDefinitionHook;
    }


    @ParameterType(".*")
    public List<City> linkedCities(String linkedCities) {
        return Arrays.stream(linkedCities.split(",")).map(cityName -> new City(CityName.valueOf(cityName.trim().toUpperCase()))).toList();
    }

    @Given("the occident network")
    public void theOccidentNetwork() {
        standardDefinitionHook.createOccidentNetwork();
    }

    @Then("the cities should have the following infection levels:")
    public void theCitiesShouldHaveTheFollowingInfectionLevels(DataTable dataTable) {
        dataTable.asMaps().forEach(dataLine -> {
                    CityName cityName = CityName.valueOf(dataLine.get("cityName").toUpperCase());
                    Assertions.assertThat(context.cities).containsKey(cityName);
                    Assertions.assertThat(context.cities.get(cityName).infectionLevel()).isEqualTo(Integer.parseInt(dataLine.get("infection level")));
                }
        );
    }

    @And("{city} should be linked to {linkedCities}")
    public void parisShouldBeLinkedToCities(City city, List<City> expectedLinkedCities) {
        List<CityName> expectedLinkedCityNames = expectedLinkedCities.stream().map(City::name).toList();
        List<CityName> linkedCityNames = context.cityLinks.stream().filter(cityLink -> cityLink.contains(city.name())).map(cityLink -> cityLink.other(city.name())).toList();

        Assertions.assertThat(linkedCityNames).containsExactlyInAnyOrderElementsOf(expectedLinkedCityNames);
    }
}
