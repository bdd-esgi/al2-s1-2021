package fr.esgi.pandemic.features;

import fr.esgi.pandemic.domain.city.City;
import fr.esgi.pandemic.domain.city.CityName;
import io.cucumber.java.Before;

import java.util.Arrays;

public class StandardDefinitionHook {

    private ScenarioContext context;

    public StandardDefinitionHook(ScenarioContext context) {
        this.context = context;
    }

    @Before("@Occident")
    public void createOccidentNetwork(){
        Arrays.stream(CityName.values()).forEach(cityName -> context.cities.put(cityName, new City(cityName)));

        context.cityLinks.add(new CityLink(CityName.PARIS, CityName.LONDON));
        context.cityLinks.add(new CityLink(CityName.PARIS, CityName.ESSEN));
        context.cityLinks.add(new CityLink(CityName.PARIS, CityName.MILAN));
        context.cityLinks.add(new CityLink(CityName.PARIS, CityName.ALGIERS));
        context.cityLinks.add(new CityLink(CityName.PARIS, CityName.MADRID));

        context.cityLinks.add(new CityLink(CityName.ESSEN, CityName.LONDON));
        context.cityLinks.add(new CityLink(CityName.ESSEN, CityName.MILAN));

        context.cityLinks.add(new CityLink(CityName.MADRID, CityName.LONDON));
        context.cityLinks.add(new CityLink(CityName.MADRID, CityName.ALGIERS));
    }
}
