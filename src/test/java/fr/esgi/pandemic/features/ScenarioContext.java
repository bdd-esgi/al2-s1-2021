package fr.esgi.pandemic.features;

import fr.esgi.pandemic.domain.city.City;
import fr.esgi.pandemic.domain.city.CityName;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class ScenarioContext {
    public Map<CityName, City> cities = new HashMap<>();
    public List<CityLink> cityLinks = new ArrayList<>();

}
