package fr.esgi.pandemic.features;

import fr.esgi.pandemic.domain.city.City;
import fr.esgi.pandemic.domain.city.CityName;
import io.cucumber.java.ParameterType;
import io.cucumber.java.en.Given;
import io.cucumber.java.en.Then;
import io.cucumber.java.en.When;
import org.assertj.core.api.Assertions;

public class InfectionSteps {

    private final ScenarioContext context;

    public InfectionSteps(ScenarioContext context) {
        this.context = context;
    }

    @ParameterType(".*")
    public City city(String cityName){
        return context.cities.get(CityName.valueOf(cityName.toUpperCase()));
    }

    @Given("{city} has not been infected")
    public void cityHasNotBeenInfected(City city) {
        city.setInfectionLevel(0);
    }

    @When("{city} is infected")
    public void cityIsInfected(City city) {
        city.infect();
    }

    @Then("{city}' infection level should be( increase to) {int}")
    public void parisInfectionLevelShouldBeIncreaseTo(City city, int infectionLevel) {
        Assertions.assertThat(city.infectionLevel()).isEqualTo(infectionLevel);
    }

    @Given("{city} has been infected {int} time(s)")
    public void parisHasBeenInfectedTime(City city, int time) {
        for (int i = 0; i < time; i++) {
            city.infect();
        }
    }
}
